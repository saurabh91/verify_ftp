
/*
 * @file	: ui.c
 * @brief	: UI emulator
 * @author	: Saurabh Kumar Verma(saurabhkumar.verma@vvdntech.com)
		  Vikas Sharma(vikas.sharma@vvdntech.com)
 * @copyright	: (c) Verify Systems 2013. All Rights Reserved.
 */


#include "ui.h"

/*
 * @function	: main
 * @params	: command line arguments 
 * @return	: function status
 * @brief	: this function
 */

int main(int argc, char *argv[])
{
	struct rlimit core_limit;
	int base = 10, opt;
	char *amgr_ipaddr = 0;
	char *endptr;
	uint16_t amgr_port;
	int sockfd;
	int len;
	struct sockaddr_in address;
	int result, snd_id = 0;

	/* enable core dump support */
	core_limit.rlim_cur = RLIM_INFINITY;
	core_limit.rlim_max = RLIM_INFINITY;

	if((setrlimit(RLIMIT_CORE, &core_limit)) < 0) {
			fprintf(stderr, "Setrlimit: %s\nWarning: core dumps may be truncated or "
                                "non-existant\n", strerror(errno));
	}

	 while((opt = getopt_long(argc, argv, ":i:p:h", ui_opts, NULL)) != -1) {
		switch(opt) {
			case 'i':
				amgr_ipaddr = strdup(optarg);
				break;
		
			case 'p':
				amgr_port = strtol(optarg, &endptr, base);
				if((errno==ERANGE && (amgr_port==LONG_MAX ||
							amgr_port==LONG_MIN))
					|| (errno!=0 && amgr_port==0)) {
					fprintf(stderr, "Error: [%s]\n", strerror(errno));
					exit(EXIT_FAILURE);
				}

				if(*endptr) {
					fprintf(stderr, "Invalid port number entered\n");
					exit(EXIT_FAILURE);
				}
				break;

			case 'h':
				usage();
				exit(EXIT_SUCCESS);
				break;

			case ':':
				fprintf(stderr, "option %c requires an argument\n",
					optopt);
				usage();
				exit(EXIT_FAILURE);
				break;

			case '?':
				fprintf(stderr, "unknown option: %c\n", optopt);
				usage();
				exit(EXIT_FAILURE);
				break;

			default:
				fprintf(stderr, "Command missing. Aborting!\n");
				abort();
				break;
			}
	}
	
	/* sanity check to ensure all the necessary arguments are present */
	if(!amgr_ipaddr || !amgr_port) {
		fprintf(stderr, "AMGR socket details missing\n");
		exit(EXIT_FAILURE);
	}

	/* UI command */
	for(; optind < argc; optind++) {
		fprintf(stdout, "UI command: %s\n", argv[optind]);
	}

	/* establish connection with Appliance Manager */
	if(!ui_connect(amgr_ipaddr, amgr_port)) {
		fprintf(stderr, "failed to establish connection with AMGR\n");
		goto error;
	}

	/* send UI command */
	if(!send_ui_cmd(argv[--optind])) {
		fprintf(stderr, "failed to send command\n");
		goto error;
	}else {
		if(!(ftp_init())){
			fprintf(stderr, "ftp initialization failed\n");
			goto error;
		}
	}
	
error:
	close(sockfd);
	exit(0);
}


/*
 * @function	: usage
 * @params	: none
 * @return	: none
 * @brief	: this function provides help documentation
 */

void usage()
{
	fprintf(stdout, "./ui --amgr_ipaddr <amgr_ipaddr> "
		"--amgr_port <amgr_port> [options] <command>\n\n");
	fprintf(stdout, "Options..\n");
	fprintf(stdout, "\t--amgr_ipaddr\tappliance manager IP address\n");
	fprintf(stdout, "\t--amgr_port\tappliance managet port\n");
	fprintf(stdout, "\t--help\t\thelp\n");
	fprintf(stdout, "\t<ui_command>\n");
}

/*
 * @function    : ui_connect
 * @params      :
 *      @param1 : AMGR IP address 
 *      @param2 : AMGR port
 * @return      : function status
 * @brief       : this function establishes TCP connection with AMGR 
 */
int ui_connect(char *amgr_ipaddr, uint16_t amgr_port)
{
        struct sockaddr_in amgr_addr;
	
        /* create endpoint for communication */
        if((ui_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                fprintf(stderr, "failed to open UI socket. Error: [%s]\n",
                                strerror(errno));
                return 0;
        }

        /* construct AMGR address */
        memset(&amgr_addr, 0, sizeof(amgr_addr));
        amgr_addr.sin_family = AF_INET;
        amgr_addr.sin_addr.s_addr = inet_addr(amgr_ipaddr);
        amgr_addr.sin_port = htons(amgr_port);

        /* establish connection with AMGR */
        if(connect(ui_sockfd, (struct sockaddr*)&amgr_addr, sizeof(amgr_addr)) < 0) {
                fprintf(stderr, "failed to connect to AMGR. Error [%s]\n",
                                strerror(errno));
                return 0;
        }

        fprintf(stdout, "Connection established with AMGR: [%s:%u]\n", amgr_ipaddr,
                        amgr_port);

        return 1;
}

/*
 * @function    : send_ui_cmd
 * @params      :
 *      @param1 : UI command 
 * @return      : function status
 * @brief       : this function sends the command to AMGR 
 */
int send_ui_cmd(char *cmd)
{
	int cnt;
	int ret = 0;
	str temp = {NULL, 0};
	char *start, *end, *endptr = NULL;
	//ui_cmd_t *ui_cmd;
	int ui_cmd_len = 0;

	start = end = NULL;
	ui_cmd_len = strlen(cmd);

	ui_cmd = (ui_cmd_t*)malloc(sizeof(ui_cmd_t));
	if (!ui_cmd) {
		fprintf(stderr, "No more memory\n");
		return 0;
	}
	memset(ui_cmd, 0, sizeof(ui_cmd_t));

	start = cmd;
	if (!(end = strchr(cmd, ' '))) {
		fprintf(stderr, "Incorrect command entered\n");
		goto error;
        }

	if(!(strncasecmp(cmd, "count", strlen("count")))) {
		ui_cmd->cmd = UI_COUNT_CMD;
		for(cnt = 0; cnt < 2; cnt++) {
			start = end + 1;
			if(cnt != 1) {
				if(!(end = strchr(start, ' '))) {
					fprintf(stderr, "Wrong format: count cmd\n");
					goto error;
				}
			}else {
				end = cmd + strlen(cmd);
			}
			temp.s = start;
			temp.len = (int)(end - start);
		
			if(cnt == 0) {
				strcpy(ui_cmd->file_name, temp.s);
				ui_cmd->file_name[temp.len] = '\0';
			}else if(cnt == 1) {
				strcpy(ui_cmd->user_name, temp.s);
				ui_cmd->user_name[temp.len] = '\0';
			}
		}
		
		/* check if any parameter is missing */
		if(!ui_cmd->file_name[0] || !ui_cmd->user_name[0]) {
			fprintf(stderr, "Wrong format: count cmd\n");
			goto error;
		}
	}else {
		fprintf(stderr, "invalid command entered\n");
		goto error;
	}

	/* send ui command to AMGR */
	if((send(ui_sockfd, ui_cmd, sizeof(ui_cmd_t), 0)) < 0) {
		fprintf(stderr, "failed to send command to AMGR: error[%s]\n",
						strerror(errno));
		goto error;
	}
	
	ret = 1;

error:
	if(!ui_cmd) {
		free(ui_cmd);
	}
	printf("send success:%s:%s:%d\n", ui_cmd->user_name, ui_cmd->file_name, ui_cmd->cmd);	
	return ret;	
}

/*
 * @function	: ftp_init
 * @params	: none
 * @return	: function status
 * @brief	: this function transfer the file from UI to AMGR through FTP
 */
int ftp_init()
{
	int ret = 1;
	int connect_id = 0, login_id = 0, put_id = 0;
	char remote_file_name[MAX_BUF_SIZE], local_file_name[MAX_BUF_SIZE];
	netbuf *ntbuf;
	ftp_details_t *ftp_details;
	ftp_details = (ftp_details_t *)malloc(sizeof(ftp_details_t));
	memset(ftp_details, 0, sizeof(ftp_details_t));

	/* load the ftp server related details */
	if(!load_ftp_configfile(ftp_details)) {
		fprintf(stderr, "error in loading ftp config file\n");
		return 0;
	}
	
	log_file_details_t *log_file_details;
	log_file_details = (log_file_details_t *)malloc(sizeof(log_file_details_t));
	
	/* loads the ui and amgr data paths */
	if(!load_ui_configfile(log_file_details)) {
		fprintf(stderr, "error in loading ui config_file\n");
		return 0;
	}

	sprintf(local_file_name, "%s/%s", log_file_details->ui_file_path, ui_cmd->file_name);
	sprintf(remote_file_name, "%s/%s", log_file_details->amgr_file_path, ui_cmd->file_name);	

	/* initialize ftp related library */
	FtpInit();

	/* connect to a ftp server */
	if(!(connect_id = FtpConnect(ftp_details->host_name, &ntbuf))) {
		fprintf(stderr, "ftp connect failed: error[%s]\n", strerror(errno));
		goto error;
	}

	/* login to ftp server */
	if(!(login_id = FtpLogin(ftp_details->usr, ftp_details->passwd, ntbuf))) {
		fprintf(stderr, "ftp login failed: error[%s]\n", strerror(errno));
		goto error;
	}

	/* send a file to ftp server */
	if(!(put_id = FtpPut(local_file_name, remote_file_name, FTPLIB_ASCII, ntbuf))) {
		fprintf(stderr, "ftp put failed: error[%s]\n", strerror(errno));
		goto error;
	}
	
error:
	FtpQuit(ntbuf);	
	/* sanity check */
	if(!login_id || !connect_id || !put_id) {
		return 0;
	}
	
	return ret;
}


/*
 * @function	: load_ftp_configfile
 * @params	: structure of ftp_details_t type
 * @retrun 	: function status
 * @brief	: this function load the ftp related parameters.
 */
int load_ftp_configfile(ftp_details_t *ftp_details)
{
	int ret = 1;
	char *tmp = NULL;
	FILE *fp;
	char ftp_cfg[MAX_BUF_SIZE];	
	char buf[READ_BUF_SIZE];
	char config_name[SMALL_BUF_SIZE];
	char config_value[MAX_BUF_SIZE];
	char *usr, *host, *passwd;

	if(!(tmp = getenv("UI_PATH"))) {
		fprintf(stderr, "FTP_PATH is not specified\n");
		return 0;
	}
	sprintf(ftp_cfg, "%s/cfg/ftp.cfg", tmp);

	fp = fopen(ftp_cfg, "r");
	if(!fp) {
		fprintf(stderr, "cann't open ftp config file %s: error[%s]\n", 
							ftp_cfg, strerror(errno));
		return 0;
	}

	/* read configuration parameters */
	while (fgets(buf, READ_BUF_SIZE, fp)) {
		size_t len = strlen(buf) - 1;
		while (isspace(buf[len])) {
			buf[len] = 0;
			len--;
		}

		if((sscanf(buf, "%s = %s", config_name, config_value)) == 2) {	
			if(!(strcasecmp(config_name, "user"))) {
				strcpy(ftp_details->usr, config_value);
			} else if(!(strcasecmp(config_name, "host_name"))) {
				strcpy(ftp_details->host_name, config_value);
			} else if(!(strcasecmp(config_name, "password"))) {
				strcpy(ftp_details->passwd, config_value);
			}
		}else {
			ret = 0; 
		}
	}
	
	/* sanity check */
	if(!ftp_details->usr[0] || !ftp_details->host_name[0] || !ftp_details->passwd[0]) {
		fprintf(stderr, "ftp details missing..");
		ret = 0; 
	}

	fclose(fp);
	return ret;
}

/*
 * @function 	: load_ui_configfile
 * @params	: none
 * @return 	: function status
 * @brief	: this function loads the ui config parameters
 */

int load_ui_configfile(log_file_details_t *log_file_details)
{
	int ret = 1;
	FILE *fp;
	char ui_cfg[MAX_BUF_SIZE];
	char *tmp = NULL;
	char config_name[SMALL_BUF_SIZE];
	char config_value[MAX_BUF_SIZE];
	char buf[READ_BUF_SIZE];
	
	if(!(tmp = getenv("UI_PATH"))) {
		fprintf(stderr, "UI_PATH is not specified\n");
		return 0;
	}
	
	sprintf(ui_cfg, "%s/cfg/ui.cfg", tmp);

	if(!(fp = fopen(ui_cfg, "r"))) {
		fprintf(stderr, "cann't open ui config file %s: error[%s]\n",
						ui_cfg, strerror(errno));
		return 0;
	}		
	
	while(fgets(buf, READ_BUF_SIZE, fp)) {
		size_t len = strlen(buf) - 1;
		while (isspace(buf[len])) {
			buf[len] = 0;
			len--;
		}
		
		if((sscanf(buf, "%s = %s", config_name, config_value)) == 2) {
			if(!(strcasecmp(config_name, "UI_SOURCE_PATH"))) {
				strcpy(log_file_details->ui_file_path, config_value);
			}else if(!(strcasecmp(config_name, "AMGR_DEST_PATH"))) {
				strcpy(log_file_details->amgr_file_path, config_value);
			}
		}else {
			ret = 0;
		}
	}
	
	return ret;
}
