
/*
 * @file 	: ui.h
 * @brief	: UI emulator
 * @author	: Saurabh Kumar Verma(saurabhkumar.verma@vvdntceh.com)
 * @copyrights	: (c) Verify Systems 2013. All rights Reserved.
 */


#ifndef __UI_EMULATOR_H__
#define __UI_EMULATOR_H__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <ftplib.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/resource.h>

#define UI_COUNT_CMD	(1<<0)

/* Macros */
#define MAX_BUF_SIZE		256
#define READ_BUF_SIZE		128
#define SMALL_BUF_SIZE		64
#define FILE_SIZE		32
#define MIN_SIZE		16

/*
 * @structure   : str 
 * @brief       : struct which defines string type 
 * @members     :
 *      @mem1   : holds string part 
 *      @mem2   : length of the string
 */
typedef struct {
	char *s;
	int len;
}str;

/*
 * @structure	: ui_opts
 * @brief	: structure holding command line parameters
 * @members:
 *	@mem1	: AMGR IP address
 * 	@mem2	: AMGR port
 * 	@mem3	: help
 */
struct option ui_opts[] = {
	{"amgr_ipaddr", 1, NULL, 'i'},
	{"amgr_port", 1, NULL, 'p'},
	{"help", 0, NULL, 'h'},
	{0, 0, 0, 0}
};

/*
 * @structure	: ui_cmd_t
 * @brief	: structure holding command
 * @members	:
 *	@mem1	: user name
 *	@mem2	: file name
 *	@mem3	: ui command
 */
typedef struct {
	char user_name[MIN_SIZE];
	char file_name[FILE_SIZE];
	uint16_t cmd;
}ui_cmd_t;

/*
 * @structure	: ftp_details
 * @brief	: structure holding ftp related details(as username, hostname)
 * @members	:
 *	@mem1	: host_name
 * 	@mem2	: user
 *	@mem3	: password
*/
typedef struct {
	char host_name[MIN_SIZE];
	char usr[MIN_SIZE];
	char passwd[MIN_SIZE];
}ftp_details_t;

/*
 * @structure	: log_file_details
 * @brief	: structure holding the ui source file path and amgr dest. file path
 * @members	: 
 *	@mem1	: ui file path
 * 	@mem2	: amgr file path
 */
typedef struct {
	char ui_file_path[MAX_BUF_SIZE];
	char amgr_file_path[MAX_BUF_SIZE];
}log_file_details_t;

/* Function Prototypes */
void usage(void);
int ui_connect(char *amgr_ipaddr, uint16_t amgr_port);
int send_ui_cmd(char *cmd);
int ftp_init();
int load_ftp_configfile(ftp_details_t *ftp_details);
int load_ui_configfile(log_file_details_t *log_file_details);

/* global variables */
int ui_sockfd;
ui_cmd_t *ui_cmd;

#endif /* __UI_EMULATOR_H__ */
