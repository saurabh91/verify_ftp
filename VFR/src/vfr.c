
/*
 * @file        : vfr.c
 * @brief       : VFR
 * @author      : Saurabh Kumar Verma(saurabhkumar.verma@vvdntech.com)
                  Vikas Sharma(vikas.sharma@vvdntech.com)
 * @copyright   : (c) Verify Systems 2013. All Rights Reserved.
 */

#include "vfr.h"

/*
 * @function	: main
 * @params	: none
 * @return	: function status 
 * @brief	: this function
*/
int main()
{
	struct rlimit core_limit;
	netbuf *net_buf = NULL;

	/* enable core dump support */
	core_limit.rlim_cur = RLIM_INFINITY;
	core_limit.rlim_max = RLIM_INFINITY;

	if((setrlimit(RLIMIT_CORE, &core_limit)) < 0) {
			fprintf(stderr, "Setrlimit: %s\nWarning: core dumps may be truncated or "
								"non-existant\n", strerror(errno));
	}

	/* load the configuration parameter */
	if(!load_config_file()) {
		fprintf(stderr, "cann't open vfr config file\n");
		exit(EXIT_FAILURE);
	}
	
	/* make a connection with amgr */
	if(!vfr_connect()) {
		fprintf(stderr, "failed to establish connection with AMGR\n");
		exit(EXIT_FAILURE);
	}

	if(!(cmd_process())) {
		fprintf(stderr, "command not processed\n");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}


/*
 * @function	: load_config_file
 * @params	: none
 * @return	: function status
 * @brief	: this function loads the configuartion parameter 
 */
int load_config_file()
{
	int ret = 1;
	char *tmp = NULL;
	FILE *fp;
	char vfr_cfg[MAX_BUF_SIZE];
	char buf[READ_BUF_SIZE];
	char config_name[MIN_SIZE];
	char config_value[MAX_BUF_SIZE];

	if(!(tmp = getenv("VFR_PATH"))) {
		fprintf(stderr, "VFR_PATH is not specified\n");
		return 0;
	}
	sprintf(vfr_cfg, "%s/cfg/vfr.cfg", tmp);
	
	fp = fopen(vfr_cfg, "r");
	if(!fp) {
		fprintf(stderr, "cann't open vfr config file %s: error[%s]\n",
							vfr_cfg, strerror(errno));
		return 0;
	}
	/* read configuration parameters */
	while (fgets(buf, READ_BUF_SIZE, fp)) {
		size_t len = strlen(buf) - 1;
		while (isspace(buf[len])) {
			buf[len] = 0;
			len--;
		}

		if((sscanf(buf, "%s = %s", config_name, config_value)) == 2) {
			if(!(strcasecmp(config_name, "amgr_port"))) {
                         	amgr_port = atoi(config_value);     
                        }else if(!(strcasecmp(config_name, "local_data_path"))) {
                                strcpy(vfr_data_path, config_value);
                        }else if(!(strcasecmp(config_name, "remote_data_path"))) {
				strcpy(amgr_data_path, config_value);	
			} 
                }else {
                        ret = 0;
		}
	}
         
	if(ret == 0) {
		fprintf(stderr, "vfr details(either amgr_port or data_path) missing\n");
	}       
	
	fclose(fp);
	return ret;                   
}

/*
 * @function    : vfr_connect
 * @params      : none
 * @return      : function status
 * @brief       : this function establishes TCP connection with AMGR 
 */
int vfr_connect()
{
	int ret = 1;
	int vfr_sockfd, amgr_sockfd;
        struct sockaddr_in amgr_addr;
	struct sockaddr_in cli_addr;
	int cli_len;
	char *amgr_ipaddr = NULL;
	
        /* create endpoint for communication */
        if((vfr_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
                fprintf(stderr, "failed to open VFR socket. Error: [%s]\n",
                                strerror(errno));
                return 0;
        }

        /* construct AMGR address */
        memset(&amgr_addr, 0, sizeof(amgr_addr));
        amgr_addr.sin_family = AF_INET;
        amgr_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        amgr_addr.sin_port = htons(amgr_port);

        /* establish connection with AMGR */
	if((bind(vfr_sockfd, (struct sockaddr *)&amgr_addr, sizeof(amgr_addr))) < 0) {
		fprintf(stderr, "amgr binding failed:error[%s]\n",
							strerror(errno));
		goto error;
	}        
	
	listen(vfr_sockfd, 5);
	
	printf("vfr waiting...\n");
	cli_len = sizeof(cli_addr);
	amgr_sockfd = accept(vfr_sockfd, (struct sockaddr *)&cli_addr, &cli_len);
		
	ui_cmd = (ui_cmd_t *)malloc(sizeof(ui_cmd_t));
	if(!ui_cmd) {
		fprintf(stderr, "no more memory\n");	
	}
	memset(ui_cmd, 0, sizeof(ui_cmd_t));
	/* recieve command from amgr */
	if((recv(amgr_sockfd, ui_cmd, sizeof(ui_cmd_t), 0)) < 0) {
		fprintf(stderr, "cann't recieve command from amgr:error[%s]\n", 
								strerror(errno));
		ret = 0;
		goto error1;
	}
	
	if(!(ftp_init())) {
		fprintf(stderr, "ftp initalization failed\n");
		ret = 0;
		goto error1;
	}
	
error1:
	close(amgr_sockfd);
error:
	close(vfr_sockfd);
	
        return ret;
}


/*
 * @function    : ftp_init
 * @params      : none
 * @return      : function status
 * @brief       : this function transfer the file from UI to AMGR through FTP
 */
int ftp_init()
{
        int ret = 1;
        int connect_id = 0, login_id = 0, get_id = 0;
        netbuf *ntbuf;
        ftp_details_t *ftp_details;
        ftp_details = (ftp_details_t *)malloc(sizeof(ftp_details_t));
        memset(ftp_details, 0, sizeof(ftp_details_t));

        /* load the ftp server related details */
        if(!load_ftp_configfile(ftp_details)) {
                fprintf(stderr, "error in loading ftp config file\n");
                return 0;
        }

        sprintf(local_file_name, "%s/%s", vfr_data_path, ui_cmd->file_name);
        sprintf(remote_file_name, "%s/%s", amgr_data_path, ui_cmd->file_name);

	/* initialize ftp related library */
        FtpInit();

        /* connect to a ftp server */
        if(!(connect_id = FtpConnect(ftp_details->host_name, &ntbuf))) {
                fprintf(stderr, "ftp connect failed: error[%s]\n", strerror(errno));
                goto error;
        }

        /* login to ftp server */
        if(!(login_id = FtpLogin(ftp_details->usr, ftp_details->passwd, ntbuf))) {
                fprintf(stderr, "ftp login failed: error[%s]\n", strerror(errno));
                goto error;
        }

        /* get a file to ftp server */
        if(!(get_id = FtpGet(local_file_name, remote_file_name, FTPLIB_ASCII, ntbuf))) {
                fprintf(stderr, "ftp put failed: error[%s]\n", strerror(errno));
                goto error;
        }

error:
        FtpQuit(ntbuf);
        /* sanity check */
        if(!login_id || !connect_id || !get_id) {
                return 0;
        }

        return ret;
}


/*
 * @function    : load_ftp_configfile
 * @params      : structure of ftp_details_t type
 * @retrun      : function status
 * @brief       : this function load the ftp related parameters.
 */
int load_ftp_configfile(ftp_details_t *ftp_details)
{
        int ret = 1;
        char *tmp = NULL;
        FILE *fp;
        char ftp_cfg[MAX_BUF_SIZE];
        char buf[READ_BUF_SIZE];
        char config_name[SMALL_BUF_SIZE];
        char config_value[MAX_BUF_SIZE];
        char *usr, *host, *passwd;

        if(!(tmp = getenv("VFR_PATH"))) {
                fprintf(stderr, "VFR FTP_PATH is not specified\n");
                return 0;
        }
        sprintf(ftp_cfg, "%s/cfg/ftp.cfg", tmp);

        fp = fopen(ftp_cfg, "r");
        if(!fp) {
                fprintf(stderr, "cann't open ftp config file %s: error[%s]\n",
                                                        ftp_cfg, strerror(errno));
                return 0;
        }

        /* read configuration parameters */
        while (fgets(buf, READ_BUF_SIZE, fp)) {
                size_t len = strlen(buf) - 1;
                while (isspace(buf[len])) {
                        buf[len] = 0;
                        len--;
                }
		if((sscanf(buf, "%s = %s", config_name, config_value)) == 2) {
                        if(!(strcasecmp(config_name, "user"))) {
                                strcpy(ftp_details->usr, config_value);
                        } else if(!(strcasecmp(config_name, "host_name"))) {
                                strcpy(ftp_details->host_name, config_value);
                        } else if(!(strcasecmp(config_name, "password"))) {
                                strcpy(ftp_details->passwd, config_value);
                        }
                }else {
                        ret = 0;
                }
        }

        /* sanity check */
        if(!ftp_details->usr[0] || !ftp_details->host_name[0] || !ftp_details->passwd[0]) {
                fprintf(stderr, "ftp details missing..");
                ret = 0;
        }

        fclose(fp);
        return ret;
}

/*
 * @function	: cmd_process
 * @params	: none
 * @return	: function status
 * @brief	: this function process the ui command
 */
int cmd_process()
{
	FILE *fp;
	char ch;
	int count = 0;
	
	if(ui_cmd->cmd == UI_COUNT_FILE) {
		if(!(fp = fopen(local_file_name, "r"))){
			fprintf(stderr, "cann't open file %s: error[%s]\n", local_file_name,
									 strerror(errno));
			return 0;
		}
		while((ch = fgetc(fp))!= EOF) {
			count += 1;
		}
	}
	fclose(fp);	
	printf("no of characters in %s: %d\n", ui_cmd->file_name, count);
	return 1;
}

