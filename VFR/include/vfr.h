
#ifndef __VFR_H__
#define __VFR_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <ftplib.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/resource.h>



#define SIZE		20
#define MIN_SIZE	16
#define FILE_SIZE	32
#define READ_BUF_SIZE	128
#define MAX_BUF_SIZE	256
#define SMALL_BUF_SIZE	64


#define UI_COUNT_FILE	(1<<0)

/*
 * @structure   : ui_cmd_t
 * @brief       : structure holding command
 * @members     :
 *      @mem1   : user name
 *      @mem2   : file name
 *      @mem3   : ui command
 */
typedef struct {
        char user_name[MIN_SIZE];
        char file_name[FILE_SIZE];
        uint16_t cmd;
}ui_cmd_t;

/*
 * @structure   : ftp_details
 * @brief       : structure holding ftp related details(as username, hostname)
 * @members     :
 *      @mem1   : host_name
 *      @mem2   : user
 *      @mem3   : password
*/
typedef struct {
        char host_name[MIN_SIZE];
        char usr[MIN_SIZE];
        char passwd[MIN_SIZE];
}ftp_details_t;


/* functions prototype */ 
int load_config_file(void);
int vfr_connect();
int ftp_init(void);
int load_ftp_configfile(ftp_details_t *ftp_details);
int cmd_process(void);


/* global variables */
uint16_t amgr_port;
char vfr_data_path[MAX_BUF_SIZE];
char amgr_data_path[MAX_BUF_SIZE];
ui_cmd_t *ui_cmd;
char remote_file_name[MAX_BUF_SIZE], local_file_name[MAX_BUF_SIZE];

#endif  /* __VFR_H__ */

