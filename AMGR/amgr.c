
#include "amgr.h"

int main()
{	
   	ui_cmd_t *ui_cmd;
	int server_sockfd, client_sockfd;
	int server_len, client_len, i =0;
	char *cli_addr;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;
	
	ui_cmd = (ui_cmd_t *)malloc(sizeof(ui_cmd_t));
	memset(ui_cmd, 0, sizeof(ui_cmd_t));
	/*  Create an unnamed socket for the server.  */

	server_sockfd = socket(AF_INET, SOCK_STREAM, 0);

	/*  Name the socket.  */

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	server_address.sin_port = htons(9737);
	server_len = sizeof(server_address);
	bind(server_sockfd, (struct sockaddr *)&server_address, server_len);

	/*  Create a connection queue and wait for clients.  */

	listen(server_sockfd, 5);
	while(i++ < 1) {
		char file_name[SIZE];
		printf("server waiting\n");

	/*  Accept a connection.  */

		client_len = sizeof(client_address);
		client_sockfd = accept(server_sockfd, 
					(struct sockaddr *)&client_address, &client_len);

	/*  We can now read/write to client on client_sockfd.  */

		if((recv(client_sockfd, ui_cmd, sizeof(ui_cmd_t), 0)) < 0) {
			fprintf(stderr, "recieve failed:error[%s]\n", strerror(errno));
			goto error;
		}else {
			puts(ui_cmd->file_name);
			cli_addr = inet_ntoa(client_address.sin_addr);
			printf("client address:%s\n", cli_addr);
			if(!(vfr_connect(ui_cmd))) {
				fprintf(stderr, "error in connecting with amgr\n");		
				goto error;
			}
		}
	}

error:
	close(client_sockfd);
	close(server_sockfd);
}


int vfr_connect(ui_cmd_t *ui_cmd) 
{
	int ret = 0;
	int client1_fd;
	struct sockaddr_in client1_address;

	client1_fd = socket(AF_INET, SOCK_STREAM, 0);

	client1_address.sin_family = AF_INET;
	client1_address.sin_addr.s_addr = inet_addr("192.168.1.4");
	client1_address.sin_port = htons(9740);
	
	if((connect(client1_fd, (struct sockaddr *)&client1_address, sizeof(client1_address))) < 0) {
		fprintf(stderr, "comm_server connect failed:error[%s]\n", 
								strerror(errno));
		goto error;
	}
//	fprintf("connection established with comm_server: [%s:%u]\n", );

	if((send(client1_fd, ui_cmd, sizeof(ui_cmd_t), 0)) < 0) {
		fprintf(stderr, "comm_server send failed:error[%s]\n",
							strerror(errno));
		goto error;
	}	
	
	ret = 1;
error:
	close(client1_fd);
	
	return ret;
}
