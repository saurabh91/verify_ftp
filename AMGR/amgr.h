
#ifndef __ANGR_H__
#define __AMGR_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <ftplib.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#define SIZE		20
#define MIN_SIZE	16
#define FILE_SIZE	32


/*
 * @structure   : ui_cmd_t
 * @brief       : structure holding command
 * @members     :
 *      @mem1   : user name
 *      @mem2   : file name
 *      @mem3   : ui command
 */
typedef struct {
        char user_name[MIN_SIZE];
        char file_name[FILE_SIZE];
        uint16_t cmd;
}ui_cmd_t;

int vfr_connect(ui_cmd_t *ui_cmd); 

#endif  /* __AMGR_H__ */

